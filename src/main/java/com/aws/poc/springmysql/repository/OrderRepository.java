package com.aws.poc.springmysql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aws.poc.springmysql.domains.OrderDetails;

@Repository
public interface OrderRepository extends JpaRepository<OrderDetails, Long> {

}
