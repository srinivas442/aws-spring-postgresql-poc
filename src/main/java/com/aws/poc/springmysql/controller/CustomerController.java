package com.aws.poc.springmysql.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.aws.poc.springmysql.domains.OrderDetails;
import com.aws.poc.springmysql.repository.OrderRepository;

@RestController
public class CustomerController {

	@Autowired
	OrderRepository orderRepository;
	
	@GetMapping("/")
	public String returnEmpty() {
		return "hello";
	}
	
	@GetMapping("/v1/orders")
	public List<OrderDetails> getCustomers(){
		return orderRepository.findAll();
	}
	
	@GetMapping("/v1/order/{id}")
	public OrderDetails getCustomer(@PathVariable Long id) {
		return orderRepository.getOne(id);
	}
	
	@PostMapping("v1/order/{id}")
	public void updateOrder(@RequestBody OrderDetails orderDetails, @PathVariable Long id) throws Exception{
		OrderDetails dbOrderDetails = orderRepository.findById(id)
						.orElseThrow(()->new Exception("Order not found:"+id));
		
		dbOrderDetails.setZipCode(orderDetails.getZipCode());
		dbOrderDetails.setName(orderDetails.getName());
		orderRepository.save(dbOrderDetails);
	}
	
	@PutMapping("v1/order/{id}")
	public void addOrder(@RequestBody OrderDetails orderDetails) {
		orderRepository.save(orderDetails);
	}
	
}
